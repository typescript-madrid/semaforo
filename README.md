# Semáforo TypeScript
## Enunciado
1. Crea un nuevo proyecto de TypeScript con ayuda de [este starter](https://github.com/mariogl/starter-typescript).
2. Copia los archivos de este enunciado (excepto el README.md) a la carpeta public del nuevo proyecto.
3. Añade al index.html la carga de jQuery mediante [un CDN](https://code.jquery.com/).
4. Añade al index.html la carga del JavaScript que va a generar tu proyecto. Recuerda que los .js no los generarás tú, los generará el compilador de TypeScript. Para saber en qué carpeta
va a generar el archivo .js que quieres cargar, mira el archivo de configuración tsconfig.json.
5. Haz que el script se ejecute después de haber parseado el DOM.
6. Programa una clase que controlará los semáforos, y que funcione de la siguiente manera:
 1. Al iniciarse, el semáforo de vehículos debe estar en verde y el de peatones en rojo. Los textos deben corresponderse en todo momento con los colores correspondientes.
 2. Al hacer clic en el botón "Iniciar semáforo", tienen que empezar los ciclos de luces, cambiando a cada segundo. El texto del botón cambiará a "Pausar semáforo".
 3. Al hacer clic en "Pausar semáforo", los ciclos de luces deben detenerse hasta que se pulse el botón de nuevo. El texto del botón cambiará a "Iniciar semáforo".

Para ejecutar un código cada x milisegundos:

```javascript
let intervalo: number = setInterval(function() {
    // Código que se ejecutará cada x milisegundos
}, x);
```

Para detenerlo:

```javascript
clearInterval(intervalo);
```

